

/* global Handlebars, utils, dataSource */ // eslint-disable-line no-unused-vars

{
  'use strict';

const select = {
  templateOf: {
    menuProduct: "#template-menu-product",
    },
    containerOf: {
      menu: '#product-list',
      cart: '#cart',
    },
    all: {
      menuProducts: '#product-list > .product',
      menuProductsActive: '#product-list > .product.active',
      formInputs: 'input, select',
    },
    menuProduct: {
      clickable: '.product__header',
      form: '.product__order',
      priceElem: '.product__total-price .price',
      imageWrapper: '.product__images',
      amountWidget: '.widget-amount',
      cartButton: '[href="#add-to-cart"]',
    },
    widgets: {
      amount: {
        input: 'input[name="amount"]',
        linkDecrease: 'a[href="#less"]',
        linkIncrease: 'a[href="#more"]',
      },
    },
  };

  const classNames = {
    menuProduct: {
      wrapperActive: 'active',
      imageVisible: 'active',
    },
  };

  const settings = {
    amountWidget: {
      defaultValue: 1,
      defaultMin: 0,
      defaultMax: 10,
    }
  };

  const templates = {
    menuProduct: Handlebars.compile(document.querySelector(select.templateOf.menuProduct).innerHTML),
  };


class Product{
  constructor(id, data){
    const thisProduct = this;
    thisProduct.id = id;
    thisProduct.data = data;
  
    thisProduct.renderInMenu();
    thisProduct.initAccordion()
    console.log(thisProduct)
  }
  renderInMenu(){
    const thisProduct = this;
    const generatedHTML = templates.menuProduct(thisProduct.data);
    thisProduct.element = utils.createDOMFromHTML(generatedHTML);
    const menuContainer =document.querySelector(select.containerOf.menu);
    menuContainer.appendChild(thisProduct.element);
  }

    

  
initAccordion() {
  const thisProduct = this;
  thisProduct.accordionTrigger = thisProduct.element.querySelector(select.menuProduct.clickable);
  thisProduct.accordionTrigger.addEventListener('click', function(event) {
  event.preventDefault();
  const activeProduct = document.querySelector(select.all.menuProductsActive)
    if (activeProduct !== null && activeProduct !== thisProduct.el) {
      activeProduct.classList.remove(classNames.menuProduct.wrapperActive); 
    }

  thisProduct.element.classList.toggle(classNames.menuProduct.wrapperActive); 
  });
}
}


  const app = {

   initMenu: function(){
    const thisApp = this
   
    for(let productData in thisApp.data.products){
      new Product(productData, thisApp.data.products[productData])
      
    }
   
   },

   initData: function(){
    const thisApp = this;
    thisApp.data = dataSource;

},


    init: function(){
      const thisApp = this;
    
      console.log('*** App starting ***');
      console.log('thisApp:', thisApp);
      console.log('classNames:', classNames);
      console.log('settings:', settings);
      console.log('templates:', templates);
      thisApp.initData()
      thisApp.initMenu()
    },
  };

  app.init();
}


processOrder() {
  const thisProduct = this;

  // covert form to object structure e.g. { sauce: ['tomato'], toppings: ['olives', 'redPeppers']}
  const formData = utils.serializeFormToObject(thisProduct.form);
  console.log('formData', formData);

  // set price to default price
  let price = thisProduct.data.price;

  // for every category (param)...
  for(let paramId in thisProduct.data.params) {
    // determine param value, e.g. paramId = 'toppings', param = { label: 'Toppings', type: 'checkboxes'... }
    const param = thisProduct.data.params[paramId];
    console.log(paramId, param);

    // for every option in this category
    for(let optionId in param.options) {
      // determine option value, e.g. optionId = 'olives', option = { label: 'Olives', price: 2, default: true }
      const option = param.options[optionId];
      console.log(optionId, option);

      if(formData[optionId] && )
    }
  }

  // update calculated price in the HTML
  thisProduct.priceElem.innerHTML = price;
}